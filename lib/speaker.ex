defmodule Speaker do
  alias Speaker.Request
  alias Speaker.{
    Config
  }
  @moduledoc """
  An Intercom Client.
  """

  def child_spec(opts) do
    %{
      worker: {Speaker.Supervisor, opts}
    }
  end

  @doc """
  Create Identity Verification Hash
  """
  def generate_user_hash(id) do
    key = Config.identity_verification_key()
    :crypto.hmac(:sha256, key, id)
    |> Base.encode16()
    |> String.downcase()
  end

  @doc """
  Create a user
  """
  def create_user(%{"user_id" => _user_id} = opts) do
    body = Jason.encode!(opts)
    Request.request(:post, "/users", [{"Content-Type", "application/json"}], body)
  end

  def create_user(%{"email" => _email} = opts) do
    body = Jason.encode!(opts)
    Request.request(:post, "/users", [{"Content-Type", "application/json"}], body)
  end
  def create_user(_), do: {:error, "user_id or email is required to create an intercom user."}

  @doc """
  Tag/Untag users
  """
  def tag(name, users) when is_binary(name) and is_list(users) do
    body = Jason.encode!(%{name: name, users: users})
    Request.request(:post, "/tags", [{"Content-Type", "application/json"}], body)
  end
  def tag(_, _), do: {:error, "name must be a string and users must be a list."}
end
