defmodule Speaker.Request do
  alias Speaker.{
    Config
  }
  def request(method, uri, headers, body) do
    client = Config.http_client()
    url = Config.url_base() <> uri
    token = Config.token()
    final_headers =
      put_token(headers, token)
    client.request(method, url, body, final_headers)
  end

  def put_token(headers, token) do
    header = [{"Authorization", "Bearer #{token}"}, {"Accept", "application/json"}]
    header ++ headers
  end
end