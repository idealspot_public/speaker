defmodule Speaker.Supervisor do
  @moduledoc false

  use Supervisor

  def init(opts) do
    {:ok, opts}
  end

  def start_link(opts) do
    children = [
      {Speaker.Config, opts}
    ]

    opts = [strategy: :one_for_one, name: Speaker.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
