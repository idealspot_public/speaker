defmodule Speaker.Config do
  use GenServer

  @default_table_opts [:set, :named_table, read_concurrency: true]

  @valid_settings ~w(token url_base http_client identity_verification_key)a

  @required ~w(token)a

  @optional [
    url_base: "https://api.intercom.io",
    http_client: HTTPoison,
    identity_verification_key: nil
  ]

  def http_client(config \\ __MODULE__) do
    get(config, :http_client)
  end

  def url_base(config \\ __MODULE__) do
    get(config, :url_base)
  end

  def token(config \\ __MODULE__) do
    get(config, :token)
  end

  def identity_verification_key(config \\ __MODULE__) do
    get(config, :identity_verification_key)
  end

  def init(opts) do
    otp_app = Keyword.fetch!(opts, :otp_app)
    table = Keyword.get(opts, :name, __MODULE__)
    :ets.new(table, @default_table_opts)
    config_opts = Application.fetch_env!(otp_app, Speaker)
    Enum.each(@required, fn key ->
      value = Keyword.fetch!(config_opts, key)
      set(table, key, value)
    end)
    Enum.each(@optional, fn {key, default} ->
      value = Keyword.get(config_opts, key, default)
      set(table, key, value)
    end)
    {:ok, %{config_name: table}}
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  defp set(table, key, value) when key in @valid_settings do
    :ets.insert(table, {key, value})
  end

  defp get(table, key) when key in @valid_settings do
    [{^key, value}] = :ets.lookup(table, key)
    value
  end
end