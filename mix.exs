defmodule Speaker.MixProject do
  use Mix.Project

  def project do
    [
      app: :speaker,
      version: "0.0.1",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    []
  end

  defp deps do
    [
      {:httpoison, "~> 1.3"},
      {:jason, "~> 1.0"} # Json Parser
    ]
  end
end
